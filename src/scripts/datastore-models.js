/** this cli is adapting datastore models to typescript models.
 * e.g.  node ./datastore-models.js -m ../models-generated.d.ts -o ../models/index.ts
 */
const { program } = require("commander");
const fs = require("fs");

program
  .option(
    "-m, --datastore-models <path>",
    "path of datastore models generated from amplify."
  )
  .option("-o, --output <path>", "the file where to write models")
  .option("-ro, --readonly", "set if the model properties are readonly")
  .option("-d, --debug", "show options for current cli usage");

program.parse(process.argv);
if (program.debug) {
  console.log(program.opts());
}

if (program.datastoreModels && program.output) {
  // read file datastore generated models and use callback to adapt models
  fs.readFile(program.datastoreModels, "utf8", adaptModels);
} else {
  console.log(
    "You should specify datastore-models and output (--help for more info)"
  );
}

/** search and replace what we want to remove
 * then erase and write in file output */
function adaptModels(err, fileContent) {
  if (err) {
    throw err;
  }

  // remove imports
  // remove datastore copy
  // remove constructor (used for datastore)
  fileContent = fileContent
    .replace(/import.*\n/g, "")
    .replace(/.*static copyOf.*\n/g, "")
    .replace(/.*constructor.*\n/g, "");

  // remove readonly if not setup
  if (!program.readonly) {
    fileContent = fileContent
    .replace(/(?<=  )readonly /g, "")
  }

  // write in models file (--output)
  fs.writeFile(program.output, fileContent, { flag: "w" }, function (err) {
    if (err) {
      throw err;
    }
    console.log("New models saved in " + program.output);
  });
}
