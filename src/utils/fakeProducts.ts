
export const fakeProducts: any[] = [
  {
    assignee: undefined,
    createdAt: '2020-05-12T07:30:46.297Z',

    favorite: undefined,
    id: '9bf54fc7-bdae-43ec-84c4-a3d05cdb9162',
    name: 'test filter',
    properties: [],
    score: undefined,
    status: undefined,
    supplier: undefined,
    teamId: '',
    lastUpdatedAt: '',
    deleted: false,
  },
  {
    assignee: undefined,
    createdAt: '2020-05-12T07:25:12.908Z',

    favorite: undefined,
    id: 'fa07e040-7b07-4642-938b-e4fdac9457d1',
    name: 'test 1',
    properties: [],
    score: undefined,
    status: undefined,
    supplier: undefined,
    teamId: '',
    lastUpdatedAt: '',
    deleted: false,
  },
  {
    assignee: undefined,
    createdAt: '2020-05-12T07:28:51.545Z',

    favorite: undefined,
    id: 'b1bbff41-fa01-46c1-9956-4f362f587d2a',
    name: '2',
    properties: [],
    score: undefined,
    status: undefined,
    supplier: undefined,

    teamId: '',
    lastUpdatedAt: '',
    deleted: false,
  },

  {
    assignee: undefined,
    createdAt: '2020-05-12T07:23:58.406Z',

    favorite: undefined,
    id: '05a39127-8b1a-4967-9b32-a57376c999df',
    name: '1',
    properties: [],
    score: undefined,
    status: undefined,
    supplier: undefined,
    teamId: '',
    lastUpdatedAt: '',
    deleted: false,
  },
];
