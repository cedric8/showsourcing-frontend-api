import { WatchQueryOptions, MutationOptions } from 'apollo-client';
import { DocumentNode, Location } from 'graphql';
import gql from 'graphql-tag';
import { jsonToGraphQLQuery } from 'json-to-graphql-query';

export interface QueryInfo {
  query: DocumentNode;
  queryName: string;
  variables: any;
  body: string;
  isMutation: boolean;
}

export class GqlHelper {
  /** returns the query, queryName and body of a specified query */
  static getQueryInfo(options: WatchQueryOptions | MutationOptions): QueryInfo {
    const isMutation = !!(options as any).mutation;
    const query =
      (options as WatchQueryOptions).query ||
      (options as MutationOptions).mutation;
    const variables = options.variables;
    const queryName = this.getQueryName(query);
    const body = this.getQueryBody(query);
    return { query, queryName, variables, body, isMutation };
  }

  /** gets the query name from a gql statement */
  static getQueryName(query: DocumentNode): string {
    return (query.definitions[0] as any).selectionSet.selections[0].name.value;
  }

  /** gets the content of a graphql query */
  static getQueryBody(query: DocumentNode): string {
    return (query.loc as Location).source.body;
  }

  static removeExtraFields(query: DocumentNode): DocumentNode {
    const queryBody = GqlHelper.getQueryBody(query);
    let stripedQueryBody = queryBody.replace('startedAt', '')
      // remove the last nextToken field if there is one
    if (stripedQueryBody.match(/(?<=syncProducts\([^)]*\))(.|\s)*nextToken/)){
      stripedQueryBody = stripedQueryBody.replace(/nextToken(?!(.|\n)*nextToken)/, '');
    }
    return gql(stripedQueryBody);
  }

  /** using jsonToGraphQLQuery fn to generate the aliases mutations */
  static getAliasedMutation(
    mutationQuery: DocumentNode,
    entities: any[]
  ): DocumentNode {
    const queryName = GqlHelper.getQueryName(mutationQuery);
    const query = {
      mutation: {} as any,
    };
    // we need to make a copy because we want to remove __typename recursively
    const entitiesCopy: any[] = JSON.parse(JSON.stringify(entities));

    const mutationFields = this._getMutationFields(mutationQuery);
    entitiesCopy.forEach((entity, i) => {
      const mutation: any = {
        __aliasFor: queryName,
        __args: { input: this._cleanEntityInput(entity) },
        [mutationFields]: true,
      };
      query.mutation[`e${i}`] = mutation;
    });
    return gql(jsonToGraphQLQuery(query, { pretty: true }));
  }

  /** recursive method who set fields to true for get aliased query with jsonToGraphQLQuery (keep only fields releated to entity properties) */
  static _getMutationFields(
    mutationQuery: DocumentNode,
    bracketPassed = 2
  ): any {
    let mutationFields = GqlHelper.getQueryBody(mutationQuery);
    let splitedFields = mutationFields.split('{');
    mutationFields = splitedFields.slice(bracketPassed).join('{');
    splitedFields = mutationFields.split('}');
    mutationFields = splitedFields
      .slice(0, splitedFields.length - bracketPassed)
      .join('}');
    return mutationFields;
  }

  /** we remove recursively,on all objects, __typename and propertiesMap that are not allowed for input mutations
   * we edit directly the reference of the object entityProperties
   */
  static _cleanEntityInput(entityProperties: any): any {
    if (!entityProperties) {
      return;
    }
    Object.entries(entityProperties).forEach(([field, value]) => {
      if (field === '__typename' || field === 'propertiesMap' || field === 'deleted') {
        delete entityProperties[field];
      } else if (Array.isArray(value)) {
        // if array with no values like properties: [], we pass because we can't get subFields. else we get fields of first item !
        if (value.length) {
          entityProperties[field] = value.map((subEntity) =>
            GqlHelper._cleanEntityInput(subEntity)
          );
        }
      } else if (typeof value === 'object' && value !== null) {
        if (Object.keys(value).length) {
          entityProperties[field] = GqlHelper._cleanEntityInput(value);
        }
      }
    });
    return entityProperties;
  }

  static getIntrospectionTypesQuery(key: string): DocumentNode {
    const query = gql`
      query {
        __type(name: "${key}") {
          name
          fields {
            name
            type {
              name
              kind
            }
          }
        }
      }
    `;
    return query;
  }
}
