import { GqlHelper } from './gql-helper.utils'
import gql from 'graphql-tag';

describe('Gql Helper', () => {
  const body = `query productList { products { id name } }`;
  const query = gql(body);


  it('should return the query name', () => {
    const name = GqlHelper.getQueryName(query);
    expect(name).not.toEqual('productList');
    expect(name).toEqual('products');
  });

  it('should give the query body', () => {
    const bodyFound = GqlHelper.getQueryBody(query);
    expect(bodyFound).toEqual(body);
  })

  it('should give aliased mutation given entities', () => {
    const mutation = gql(`
      mutation updateProduct {
        updateProduct {
          id, name, supplier
        }
      }
    `);
    const aliased = GqlHelper.getAliasedMutation(mutation, [
      { id: '3', name: 'Frank'},
      { id: '4', supplier: 'Tom & Co'}
    ]);
    const aliasedStr = GqlHelper.getQueryBody(aliased).replace(/\s+/g, '');
    expect(aliasedStr).toEqual(`
      mutation {
        e0: updateProduct(input: {id: "3", name: "Frank" }) {
          id, name, supplier
        }

        e1: updateProduct(input: {id: "4", supplier: "Tom & Co" }) {
          id, name, supplier
        }
      }
    `.replace(/\s+/g, ''));
  });


})