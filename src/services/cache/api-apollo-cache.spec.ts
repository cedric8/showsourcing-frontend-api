import ApolloClient, { gql } from 'apollo-boost';
import { InMemoryCache } from 'apollo-cache-inmemory';
import 'cross-fetch/polyfill';
import { ApiApolloCache } from './api-apollo-cache';
import { SyncMap } from '../sync';

describe('Api Cache', () => {
  const apolloCache = new InMemoryCache();
  const client: ApolloClient<any> = new ApolloClient<any>({
    uri: 'https://graphql.example.com',
    cache: apolloCache
  });
  const cacheKey = 'Product';
  const syncMap: SyncMap = {
    [cacheKey]: {
      syncable: true,
      base: {
        limitPaginate: 10,
        query: gql(`
        query syncProducts {
          syncProducts {
            items { id name }
          }
        }`),
        variables: {},
        __typename: 'ModelProductConnection'
      }
    }
  };
  const cache = new ApiApolloCache(syncMap);
  cache.setAppSyncClient(client as any);
  const source = [
    { id: 'x', name: 'Cedric', __typename: 'Product' },
    { id: 'y', name: 'Augustin', __typename: 'Product' },
    { id: 'a', name: 'Dr. Smith', __typename: 'Product' }
  ];

  const updates = [
    { id: 'x', name: 'Cedric', deleted: true, __typename: 'Product' }, // fired
    { id: 'y', name: 'Augustin_Pussy_destroyer', __typename: 'Product' }, // promoted
    { id: 'z', name: 'new asian guy', __typename: 'Product' } // joined
  ];
  const expectedResult = [
    { id: 'z', name: 'new asian guy', __typename: 'Product' },
    { id: 'y', name: 'Augustin_Pussy_destroyer', __typename: 'Product' },
    { id: 'a', name: 'Dr. Smith', __typename: 'Product' }
  ];

  it('should insert & read cache at specified key', () => {
    cache.insert(cacheKey, source);
    const r = cache.read(cacheKey);
    expect(r).toEqual(source);
  });

  it('should reset list in cache at specified key', () => {
    cache.insert(cacheKey, source);
    cache.reset(cacheKey);
    const r = cache.read(cacheKey);
    expect(r).toEqual([]);
  });

  it('should generate an upsert result when given two arrays', () => {
    const result = cache.getUpsertResult(source, updates);
    expect(result.items).toEqual(expectedResult);
  }),
    it('should generate an upsert report when given two arrays', () => {
      const result = cache.getUpsertResult(source, updates);
      const report = result.report;
      expect(report.deleted.length).toBe(1);
      expect(report.inserted.length).toBe(1);
      expect(report.updated.length).toBe(1);
    });

  it('should do the upsert', async () => {
    cache.reset(cacheKey);
    cache.insert(cacheKey, source);
    cache.upsert(cacheKey, updates);
    const items = cache.read(cacheKey);
    expect(items).toEqual(expectedResult);
  });

  it('should watch list in cache at specified key', done => {
    cache.reset(cacheKey);
    cache.insert(cacheKey, source);
    let i = 0;
    cache.observe(cacheKey).subscribe(items => {
      if (i === 0) {
        expect(items).toEqual(source);
      }
      if (i === 1) {
        expect(items).toEqual(expectedResult);
        done();
      }
      i++;
    });
    setTimeout(() => cache.upsert(cacheKey, updates), 50);
  });
});
