import { EntityFields } from '../db/entity.interface';
import { Observable } from 'rxjs';

export interface UpdateReport {
  inserted: EntityFields[];
  deleted: EntityFields[];
  updated: EntityFields[];
}

export interface CacheData {
  [key:string]: {
    items: EntityFields[];
    __typename: string;
    startedAt: number;
  }
}

export interface ApiCache {
  read(key: string): EntityFields[];
  insert(key: string, items: EntityFields[], fieldsref: any): void;
  reset(key: string): void;
  upsert(key: string, items: EntityFields[]): UpdateReport;
  observe(key: string): Observable<EntityFields[]>;
}
