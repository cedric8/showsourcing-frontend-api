

import { DocumentNode } from 'graphql';


export interface MutationMap {
  [key: string]: {
    create?: DocumentNode;
    update?: DocumentNode;
    delete?: DocumentNode;
  };
}