import { from, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { GqlHelper } from '../../utils/gql-helper.utils';
import { AbstractAppsyncWrap } from '../abstract-sync-wrap.class';
import { EntityFields } from '../db/entity.interface';
import { SyncMap } from '../sync/sync-map.interface';
import { ApiCache, CacheData, UpdateReport } from './api-cache.interface';
import { log } from 'simply-logs';


export class ApiApolloCache extends AbstractAppsyncWrap implements ApiCache {

  // TODO we can improve perfs here by caching a map
  // with { id: index }

  constructor(
    private syncMap: SyncMap,
  ) {
    super();
  }

  /** to read once from the cache */
  read(key: string): any[] {
    const { query, variables } = this.syncMap[key].base;
    const queryName = GqlHelper.getQueryName(query);
    const r: CacheData | null = this.client.readQuery({ query, variables });
    return r ? r[queryName]?.items : [];
  }

  /** to insert cache data in cache */
  insert(key: string, items: any[]) {
    const { query, variables, __typename } = this.syncMap[key].base;
    const queryName = GqlHelper.getQueryName(query);
    const cleanedQuery = GqlHelper.removeExtraFields(query);
    this.client.writeQuery({ query: cleanedQuery, variables, data: { [queryName]: { items, __typename } } });
  }

  /** to reset cache at specified key, cache will look like { items: [] } after */
  reset(key: string) {
    const { query, variables, __typename } = this.syncMap[key].base;
    const queryName = GqlHelper.getQueryName(query);
    this.client.writeQuery({ query, variables, data: { [queryName]: { items: [], __typename } } });
  }

  /** observe cacheModel at key */
  observe(key: string): Observable<any[]> {
    const { query, variables } = this.syncMap[key].base;
    const queryName = GqlHelper.getQueryName(query);

    return from(
      this.client
        .watchQuery({
          fetchPolicy: 'cache-only',
          query,
          variables
        }) as any,
    ).pipe(
      // not sure about this line, but else we get more
      // than one result
      filter((r: any) => r.networkStatus === 7),
      map((r: any) => r.data[queryName]?.items || []),
    );
  }

  /**
   * upsert items into a specific cache list under key
   * @param key on which the data list is stored
   * @param items items to insert
   * @returns UpdateReport with informations about the upsert
   */

  upsert(key: string, updates: EntityFields[]): UpdateReport {
    const source = this.read(key);
    const { report, items } = this.getUpsertResult(source, updates);
    const descendants = this.getDescendants(updates);
    Object.entries(descendants).forEach(([k, v]) => {
      if (!this.syncMap[k].syncable) {
        this.upsert(k, v);
      }
    });
    this.insert(key, items);
    log.debug('cache upsert report', report, 'items', items)
    return report;
  }

  /**
   * makes an upsert of updates on a source array
   * @param source array of items to be updated
   * @param updates array of updates
   * @returns the resulting array as well as a report of the upsert
   */
  getUpsertResult(source: EntityFields[] = [], updates: EntityFields[] = [])
    : { items: EntityFields[], report: UpdateReport }  {
    const deleted: EntityFields[] = [];
    const updated: EntityFields[] = [];
    const inserted: EntityFields[] = [];
    const cachedItemsMap: Record<string, EntityFields> = {};
    source.forEach(item => cachedItemsMap[item.id] = item);

    updates.forEach(item => {
      if (item.deleted === true) {
        deleted.push(item);
        delete cachedItemsMap[item.id];
      }
      else if (cachedItemsMap[item.id]) {
        updated.push(item);
        cachedItemsMap[item.id] = {...cachedItemsMap[item.id], ...item};
      } else {
        inserted.push(item);
        // inserted is added below so we can add it to the beginning of the array
      }
    });
    const items = [...inserted, ...Object.values(cachedItemsMap)];
    return { items, report: { inserted, updated, deleted }};
  }

  private getDescendants(entities: any[]) {
    // we go one level { typename: toInsert[] }
    const toInsert: { [key: string]: any[] } = {};
    entities.forEach(entity =>
      // we check the nested fields and if they have typename we also insert them in a cached list
      // so we can retrieve that list too
      Object.values(entity).forEach(v => {
        if (Array.isArray(v) && v[0] &&  v[0].__typename) {
          const typename = v[0].__typename;
          // create empty array if it's undefined
          toInsert[typename] = [...(toInsert[typename] || []), ...v];
        } else if (typeof v === 'object' && v !== null && (v as any).__typename) {
          const typename = (v as any).__typename;
          toInsert[typename] = [...(toInsert[typename] || []), v];
        }
      })
    );
    // Object.entries(toInsert).forEach(([k, v]) => this.insert(k, v));
    return toInsert;
  }

}
