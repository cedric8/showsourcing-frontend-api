import { AwsExport } from './aws-export.interface';
import { SyncMap } from '../sync';
import { MutationMap } from '../cache/mutation-map.interface';
import { LogLevel } from 'simply-logs';
import { BehaviorSubject } from 'rxjs';

export * from 'simply-logs/dist/log-level';

export interface Config {
  awsExport: AwsExport;
  syncMap: SyncMap;
  mutationMap: MutationMap;
  isOnline$: BehaviorSubject<boolean>
  logLevel?: LogLevel;
}
