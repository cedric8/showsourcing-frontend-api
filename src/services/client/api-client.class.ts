import { AuthClass } from '@aws-amplify/auth/lib-esm/Auth';
import { Amplify, Auth, Hub, Storage, StorageClass, auth0SignInButton } from 'aws-amplify';
import { AUTH_TYPE, AWSAppSyncClient } from 'aws-appsync';
import { ReplaySubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { log, LogLevel } from 'simply-logs';
import '../../utils/polyfill.utils';
import { ApiApolloCache } from '../cache/api-apollo-cache';
import { ApiDB } from '../db/api-db.class';
import { ApiSynchronizer } from '../sync/api-synchronizer.class';
import { AwsExport } from './aws-export.interface';
import { Config } from './config.interface';
import { Observable } from 'rxjs';
import { GqlHelper } from '../../utils/gql-helper.utils';

import localForage from 'localforage';

localForage.config({
    driver      : localForage.INDEXEDDB, // Force WebSQL; same as using setDriver()
    name        : 'apiLib',
    version     : 1.0,
    // size        : 4980736, // Size of database, in bytes. WebSQL-only for now.
    storeName   : 'appsync', // Should be alphanumeric, with underscores.
    description : 'entities stored locally with apollo appsync for the showsourcing app'
});

export class ApiClient {

  private _appSyncClient: AWSAppSyncClient<any> | undefined;
  private _cache: ApiApolloCache;
  private _synchronizer: ApiSynchronizer;
  private _db: ApiDB;
  private _user$ = new ReplaySubject(1);
  user$: Observable<any> = this._user$.asObservable();
  private signin$ = this.user$.pipe(filter(user => !!user));
  private signout$ = this.user$.pipe(filter(user => !user));
  private _ready$ = new ReplaySubject<boolean>(1);
  ready$: Observable<boolean> = this._ready$.asObservable();

  constructor(private config: Config) {
    log.setLogLevel(config.logLevel || LogLevel.DEBUG);
    Amplify.configure(config.awsExport);
    // setup services so they are never undefined and
    //  we can import and export them via const db = client.db
    this._cache = new ApiApolloCache(this.config.syncMap);
    this._synchronizer = new ApiSynchronizer(this._cache, this.config);
    this._db = new ApiDB(this.config.mutationMap, this._cache, this._synchronizer);
    // check if user is authenticated
    this.checkAuth();
    // subscribe to future auth
    this.listenAuth();
    // 1. On sign In start aws client
    this.signin$.subscribe(_ => this.startServices())
    // 2. on logout reset store
    this.signout$.subscribe(_ => this.stopServices());
  }

  private startServices() {
    // check if already instantiated because we want only one instance
    this._appSyncClient = this._appSyncClient || this.getAwsClient(this.config.awsExport);
    this._cache.setAppSyncClient(this._appSyncClient as AWSAppSyncClient<any>);
    this._synchronizer.setAppSyncClient(this._appSyncClient as AWSAppSyncClient<any>);
    this._db.setAppSyncClient(this._appSyncClient as AWSAppSyncClient<any>);
    // we hydrate the cache for old data
    this._appSyncClient.hydrated()
    .then(_ => this._preWriteCache())
    .then(_ => this._appSyncClient?.initQueryManager())
    // we sync
    .then(_ => this._synchronizer.sync())
    // we ready
    .then(_ => this._ready$.next(true));
  }

  private stopServices() {
    // this._appSyncClient?.clearStore();
    this._appSyncClient = undefined;
    this._ready$.next(false);
  }

  private checkAuth() {
    // check for current authenticated user to init authState
    Auth.currentAuthenticatedUser()
      .then(user => this._user$.next(user))
      .catch(err => this._user$.next(undefined));
  }

  private listenAuth() {
    // this won't give the result for initial authentication if the user is already logged in
    Hub.listen('auth', ({ channel, payload }) => {
      if (channel === 'auth') {
        if (payload.event === 'signIn') {
          const user = payload.data;
          this._user$.next(user);
        }
        if (payload.event === 'signOut') {
          this._user$.next(undefined);
        }
      }
    });
  }

  private getAwsClient(awsExport: AwsExport): AWSAppSyncClient<any> {
    return new AWSAppSyncClient({
      url: awsExport.aws_appsync_graphqlEndpoint,
      region: awsExport.aws_appsync_region,
      auth: {
        type: AUTH_TYPE.AMAZON_COGNITO_USER_POOLS,
        jwtToken: async () => (await Auth.currentSession()).getIdToken().getJwtToken(),
      },
      offlineConfig: {
        storage: localForage
      }
    });
  }

  get appSyncClient(): AWSAppSyncClient<any> | undefined {
    return this._appSyncClient;
  }

  get cache(): ApiApolloCache {
    return this._cache;
  }

  get db(): ApiDB {
    return this._db;
  }

  /** Amplify Auth */
  get Auth(): AuthClass {
    return Auth;
  }

  /** Amplify Storage */
  get storage(): StorageClass {
    return Storage;
  }

  get synchronizer(): ApiSynchronizer {
    return this._synchronizer;
  }

  // help to pre write the cache with empty queries to avoid errors if the key does not exist
  private _preWriteCache() {
    Object.values(this.config.syncMap).forEach((entityBase) => {
      const { query, variables, __typename } = entityBase.base
      const queryName = GqlHelper.getQueryName(query);
      try {
        this._appSyncClient?.readQuery({ query, variables});
      } catch (err) {
        this._appSyncClient?.writeQuery({ variables , query, data: {[queryName]: {
          __typename,
          items: [],
          startedAt: 0,
          nextToken: null
        }}})
      }
    })
  }

}
