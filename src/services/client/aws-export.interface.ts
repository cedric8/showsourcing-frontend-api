

export interface AwsExport {
  aws_appsync_graphqlEndpoint: string;
  aws_appsync_region: string;
}