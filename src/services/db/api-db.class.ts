import { DocumentNode } from 'graphql';
import { combineLatest, Observable, of, Subject, from } from 'rxjs';
import { first, map, switchMap, tap, shareReplay } from 'rxjs/operators';
import { log } from 'simply-logs';
import { GqlHelper } from '../../utils/gql-helper.utils';
import { AbstractAppsyncWrap } from '../abstract-sync-wrap.class';
import { ApiApolloCache } from '../cache/api-apollo-cache';
import { UpdateReport } from '../cache/api-cache.interface';
import { MutationMap } from '../cache/mutation-map.interface';
import { ApiFilter } from '../filter/api-filter.class';
import { FilterType, PaginationType, SortType } from '../filter/filter.type';
import { ApiSynchronizer } from '../sync/api-synchronizer.class';
import { DbLogger } from './db-logger.class';
import { EntityFields } from './entity.interface';

export interface ApiResponse<T = EntityFields> {
  data$: Observable<T[]>;
  count$: Observable<number>;
}

export class ApiDB extends AbstractAppsyncWrap {
  constructor(
    private mutationMap: MutationMap,
    private apiCache: ApiApolloCache,
    private synchronizer: ApiSynchronizer
  ) {
    super();
  }
  private apiFilter = new ApiFilter();

  async mutate(
    key: string,
    updates: EntityFields[],
    mutation: DocumentNode,
    mutationType: 'create' | 'update' | 'delete'
  ): Promise<UpdateReport> {
    // we read the current state of cache
    const currentCache = this.apiCache.read(key);

    if (mutationType === 'create') {
      // we only need the empty fields (optimistic) at create as for update/delete, we get items fields from cache
      const optimisticCreate = await this._getEmptyFieldsEntity(key);
      updates = updates.map((item) => Object.assign(optimisticCreate, item));
    }
    // TODO change log mutation
    DbLogger.logMutation(key, mutation, currentCache, updates);
    const report = this.apiCache.upsert(key, updates);
    DbLogger.logMutationCache(key, report);

    this.client
      .mutate<EntityFields>({
        mutation,
        update: this._getUpdateMutationFn(key, () =>
          this._revertCache(key, currentCache, mutationType)
        ),
      })
      .then((r) => DbLogger.logMutationServer(mutation, r))
      .catch((err) => {
        log.error(err);
        // we revert changes
        this._revertCache(key, currentCache, mutationType);
      });
    return report;
  }

  /** get one entity by id */
  get(key: string, id: string): Observable<any> {
    DbLogger.logGet(key, id);
    // We don't want to trigger fetchDelta for each Get relation
    // this.synchronizer.fetchDelta(key).pipe(first()).subscribe();
    return this.apiCache.observe(key).pipe(
      map((items) => items.find((item) => item.id === id)),
      map((item) => this._deserializeProperties([item])[0]),
      tap((items) => DbLogger.logGetLocalResult(key, items))
    );
  }

  /**
   * find entities in the cache
   * @param key: the type you want to find
   * @param filters: recursive object to filter entities e.g. {and: [{property: 'name', contains: 'hello'}, ...]}
   * @param pagination: the slice of entities e.g. {page: 0, limit: 25}
   * @param sort: how the entities are sorted e.g. {direction: 'ASC', property: 'name'}
   * @returns a single response (first()) trough an observable.
   */
  find(
    key: string,
    sort?: SortType,
    filters?: FilterType,
    pagination: PaginationType = { page: 0, limit: 25 }
  ): ApiResponse<EntityFields> {
    DbLogger.logFind(key, sort, filters, pagination);
    this.synchronizer.fetchDelta(key).pipe(first()).subscribe();
    // TODO count$ & data$ are not shared, so if you subscribe to
    // count$ + data$ the log will log twice (the pipe goes through twice)

    const filteredData$ = this.apiCache.observe(key).pipe(
      map((items) => this.apiFilter.filter(items, filters) as any[]),
      tap((_) => console.log('FILTERED')) // logged 2 times..
    );

    const apiResponse: ApiResponse<any> = {
      count$: filteredData$.pipe(map((items) => items.length)),
      data$: filteredData$.pipe(
        map((filteredItems) => this.apiFilter.sort(filteredItems, sort)),
        map((sortedItems) => this.apiFilter.paginate(sortedItems, pagination)),
        map((paginedItems) => this._deserializeProperties(paginedItems)),
        tap((items) => DbLogger.logFindLocalResult(key, items))
      ),
    };
    return apiResponse;
  }

  find$(
    key: string,
    sort$: Observable<SortType>,
    filters$: Observable<FilterType>,
    pagination$: Observable<PaginationType> = of({ page: 0, limit: 25 })
  ): ApiResponse<EntityFields> {
    const apiResponse = combineLatest([sort$, filters$, pagination$]).pipe(
      map(([sort, filters, pagination]) =>
        this.find(key, sort, filters, pagination)
      )
    );

    const data$ = apiResponse.pipe(switchMap((resp) => resp.data$));
    const count$ = apiResponse.pipe(switchMap((resp) => resp.count$));

    return { data$, count$ };
  }

  /** update many entities in backend and update the cache */
  update(key: string, entities: EntityFields[]): Observable<EntityFields[]> {
    const updateFn = this._getMutationFn('update');
    return from(updateFn(key, entities)).pipe(
      map((repport) => repport.updated)
    );
  }

  /** delete many entities in backend and update the cache */
  delete(key: string, entities: EntityFields[]): Observable<EntityFields[]> {
    const updateFn = this._getMutationFn('delete');
    entities.forEach((entity) => (entity.deleted = true));
    return from(updateFn(key, entities)).pipe(
      map((repport) => repport.deleted)
    );
  }

  /** create many entities in backend and update the cache */
  create(key: string, entities: EntityFields[]): Observable<EntityFields[]> {
    const updateFn = this._getMutationFn('create');
    return from(updateFn(key, entities)).pipe(
      map((repport) => repport.inserted)
    );
  }

  private _getMutationFn(mutationType: 'create' | 'update' | 'delete') {
    return (key: string, entities: EntityFields[]): Promise<UpdateReport> => {
      const mutations = this.mutationMap[key];
      if (!(mutations && mutations[mutationType])) {
        throw Error(
          `the mutation ${mutationType} doesn't exist for the key '${key}'`
        );
      }
      entities = this._serializeProperties(entities);
      const mutation = GqlHelper.getAliasedMutation(
        mutations[mutationType] as DocumentNode,
        entities
      );

      return this.mutate(key, entities, mutation, mutationType);
    };
  }

  /** method who update the cache in appolo client for the aliased mutation */
  private _getUpdateMutationFn(key: string, callRevertCache: any) {
    return (cache: any, resp: any) => {
      // data contains {[aliases]: entity}
      const items: EntityFields[] = Object.values(resp.data);
      if (items.findIndex((i) => i === null) !== -1) {
        // if we are offline, we get null as response and don't want to write it into the cache.
        return;
      }
      if (items.length === 0) {
        callRevertCache();
      } else {
        this.apiCache.upsert(key, items);
      }
    };
  }

  /** add a property "propertiesMap" on entity (use this when we get entities).
   * propertiesMap contains all "properties" converted into an object with values parsed. [{name, value}, ...] => {[name]: JSON.parse(value), ...}
   */
  private _deserializeProperties(entities: EntityFields[]) {
    entities.forEach((entity: EntityFields) => {
      if (entity && Array.isArray(entity.properties)) {
        const propertiesMap: any = {};
        entity.properties.forEach((prop: any) => {
          propertiesMap[prop.name] = JSON.parse(prop.value);
        });
        entity.propertiesMap = propertiesMap;
      }
    });
    return entities;
  }

  /** replace the properties array with the updated values from propertiesMap (use this before insert entities) */
  private _serializeProperties(entities: EntityFields[]) {
    entities.forEach((entity: EntityFields) => {
      if (entity.propertiesMap && Object.keys(entity.propertiesMap).length) {
        const propertiesArray: any = [];
        Object.entries(entity.propertiesMap).forEach(([name, value]) => {
          propertiesArray.push({
            name,
            value: JSON.stringify(value),
            __typename: 'Property',
          });
        });
        entity.properties = propertiesArray;
      }
    });
    return entities;
  }

  private async _getEmptyFieldsEntity(key: string) {
    const query = GqlHelper.getIntrospectionTypesQuery(key);
    const res: any = await this.client.query({
      query,
      fetchPolicy: 'cache-only',
    });
    const optimisticObject: any = {};
    res?.data?.__type?.fields.forEach((f: any) => {
      const kind = f.type?.kind;
      if (kind === 'LIST') {
        optimisticObject[f.name] = [];
      } else if (kind !== 'NON_NULL') {
        optimisticObject[f.name] = null;
      } else if (f.name === 'deleted') {
        // this kind is required (NON_NULL) but removed from query input (custom backend)
        optimisticObject[f.name] = null;
      }
    });
    return optimisticObject;
  }

  private _revertCache(key: string, oldEntities: EntityFields[], mutationType: string) {
    log.warn('reverting cache to old state');
    if (mutationType) {
      // to remove items created, we delete them from cache
      oldEntities.forEach(entity => entity.deleted = true);
    }
    this.apiCache.upsert(key, oldEntities);
  }
}
