

describe('Api DB', () => {

  // GET
  it('should get one entity', () => {
    // TODO
  });

  it('should receive changes when the entity we got is updated', () => {
    // TODO
  });

  // find
  it('should find entity based on filters, pagination and sort', () => {
    // TODO
  });

  it('should emit changes when found entities are updated', () => {
    // TODO
  });

  // find$
  it('should find entity and react based on filters$, pagination$ and sort$', () => {
    // TODO
  });

  it('should emit changes when found$ entities are updated', () => {
    // TODO
  });

  // create
  it('should create the entities in the cache', () => {
    // TODO
  });

  it('should emit a graphql query to create', () => {
    // TODO
  });

  it('should revert creation if query fails', () => {
    // TODO
  });

  // update
  it('should update the entities in the cache', () => {
    // TODO
  });

  it('should emit a graphql query to update', () => {
    // TODO
  });

  it('should revert update if query fails', () => {
    // TODO
  });

  // delete
  it('should delete the entity in the cache', () => {
    // TODO
  });

  it('should emit a graphql query to delete (deleted = true)', () => {
    // TODO
  });

  it('should revert deletion if query fails', () => {
    // TODO
  });

});