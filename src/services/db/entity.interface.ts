

export interface EntityFields {
  id: string;
  deleted?: boolean;
  [field: string]: any;
}
