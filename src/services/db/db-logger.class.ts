

import { DocumentNode } from 'graphql';
import { log, Log, LogLevel, prettyBrowser } from 'simply-logs';
import { GqlHelper } from '../../utils/gql-helper.utils';
import { UpdateReport } from '../cache/api-cache.interface';
import { FilterType, PaginationType, SortType } from '../filter/filter.type';

/** colors */
const COLOR_LOCAL = 'color: gold; background: #3b2d44; padding: 4px';
const COLOR_LOCAL_RESPONSE = 'color: lime; background: #3b2d44; padding: 4px';
const COLOR_SERVER_RESPONSE = 'color: pink; background: #3b2d44; padding: 4px';
/** icons */
const iconQuery = '🍌';
const iconMutation = '🍇';

// adds log level tag at the start of a log
log.transformFn = prettyBrowser;
// adding a second logger that hasn't the log level at the start
const simpleLog = new Log();
simpleLog.setLogLevel(log.getLogLevel());

export class DbLogger {

    /** logs request that is about to being made to the 	 */
  static logMutation(
    key: string,
    mutation: DocumentNode,
    source: any,
    updates: any
  ) {
    // logging for each request
    const body = GqlHelper.getQueryBody(mutation);
    log.group(LogLevel.INFO, `%c${iconMutation} Mutation on ${key}`, COLOR_LOCAL);

    simpleLog.info('source', source);
    simpleLog.info('updates', updates);

    simpleLog.group(LogLevel.INFO, 'gql');
    simpleLog.info(body);
    simpleLog.groupEnd(LogLevel.INFO);

    simpleLog.group(LogLevel.INFO, 'trace');
    simpleLog.trace(LogLevel.INFO);
    simpleLog.groupEnd(LogLevel.INFO);

    log.groupEnd(LogLevel.INFO);
  }

  static logMutationCache(key: string, report: UpdateReport) {
    log.group(LogLevel.INFO, `%c${iconMutation} Result Mutation on ${key}`, COLOR_LOCAL_RESPONSE);
    simpleLog.info('report', report);
    log.groupEnd(LogLevel.INFO)
  }


  /** logs data received  */
  static logMutationServer(mutation: DocumentNode, result: any) {
    const queryName = GqlHelper.getQueryName(mutation);
    log.info(`%c${iconMutation} Server Result Mutation on ${queryName}`, COLOR_SERVER_RESPONSE, result);
  }

  static logGet(key: string, id: string) {
    log.group(LogLevel.INFO,`%c${iconQuery} Get ${key} id: ${id}`, COLOR_LOCAL);
    simpleLog.info('id', id);
    simpleLog.group(LogLevel.INFO, 'trace');
    simpleLog.trace(LogLevel.INFO);
    simpleLog.groupEnd(LogLevel.INFO);
    log.groupEnd(LogLevel.INFO);
  }

  static logGetLocalResult(key: string, item: any) {
    log.group(LogLevel.INFO, `%c${iconQuery} Result ${key} (${item ? 1 : 0})`, COLOR_LOCAL_RESPONSE);
    log.info('item', item);
    log.groupEnd(LogLevel.INFO);
  }

  static logFind(
    key: string,
    sort?: SortType,
    filters?: FilterType,
    pagination?: PaginationType) {
    log.group(LogLevel.INFO,`%c${iconQuery} Find ${key}`, COLOR_LOCAL);
    simpleLog.info('sort', sort);
    simpleLog.info('filters', filters);
    simpleLog.info('pagination', pagination);
    simpleLog.group(LogLevel.INFO, 'trace');
    simpleLog.trace(LogLevel.INFO);
    simpleLog.groupEnd(LogLevel.INFO);
    log.groupEnd(LogLevel.INFO);
  }

  static logFindLocalResult(key: string, items: any) {
    log.group(LogLevel.INFO, `%c${iconQuery} Result ${key} (${items.length})`, COLOR_LOCAL_RESPONSE);
    log.info('items', items);
    log.groupEnd(LogLevel.INFO);
  }

}