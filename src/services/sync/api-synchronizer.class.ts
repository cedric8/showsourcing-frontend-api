import { ApolloQueryResult, QueryOptions } from 'apollo-client';
import { from, Observable, of } from 'rxjs';
import { log } from 'simply-logs';
import { GqlHelper } from '../../utils/gql-helper.utils';
import { ApiApolloCache } from '../cache/api-apollo-cache';
import { CacheData } from '../cache/api-cache.interface';
import { BaseQueryOptions, SyncMap } from './sync-map.interface';
import { AbstractAppsyncWrap } from '../abstract-sync-wrap.class';
import { Config } from '../client/config.interface';

export class ApiSynchronizer extends AbstractAppsyncWrap {
  private subscriptions: any[] = [];

  syncMap: SyncMap;
  /** the name of the cache key who store the lastSync map */
  constructor(private apiCache: ApiApolloCache, public config: Config) {
    super();
    this.syncMap = config.syncMap;
  }

  /** setup base sync and delta sync
   * should be used once at app initialization
   * the minimum fields of queries are '{ items { id deleted } startedAt }'
   * @returns array subscription: to unsubscribe when we want to stop the syncronization with the backend.
   */
  async sync(): Promise<any> {
    // generate sync subscriptions to return.
    log.debug('sync', this.syncMap);
    this.removeOldSubscriptions();
    const finished: Promise<any>[] = [];
    Object.entries(this.syncMap)
      .filter(([_, sync]) => sync.syncable)
      .forEach(([key, sync]) => {
      finished.push(
        new Promise((resolve, reject) => {
          // we build BaseQuery objects to sync
          const baseQuery: BaseQueryOptions = { ...sync.base };
          const countPaginate = this.syncMap[key].base.limitPaginate;
          const updateFn = this._getUpdateFn(key, countPaginate);
          // If the sync has already been done in the past app sync won't relaunch the sync
          // and therefor won't relaunch the update function. To know if it was done before
          // we can check the cache
          try {
            const cached = this.client.readQuery({ ...sync.base });
            if (Object.values(cached as {})[0]) {
              resolve();
            } else {
              // reject();
            }
            // else sync has been done in the past but was cleared by resetStore
          } catch (e) {
            log.error(e);
            // reject();
          }

          baseQuery.update = (_, resp) => {
            resolve();
            return updateFn(_, resp);
          };
          this.subscriptions.push(this.client.sync({ baseQuery } as any));
        })
      );
    });
    return await Promise.all(finished);
  }

  /** throw a sync query (related to cacheKey) to update the cache with the latest products.
   * keep lastSync variable up to date in the cache. (reduxPersist::appsync-metadata => deltaSync => metadata)
   * @param cacheKey: the key to retrive the query to use and update the cache.
   * @param nextToken:
   * @param countPaginate:
   * @return Observable: the response of the delta query => client.query()
   */
  fetchDelta<T = any>(key: string, nextToken?: string, countPaginate?: number): Observable<ApolloQueryResult<T> | null> {
    if (this.config.isOnline$.getValue() === false) {
      log.warn(`Delta not fetched as you are offline.`);
      return of(null);
    }
    const lastSync: number = this._updateLastSync(key) || 0;
    log.debug(`Delta ${key}, lastSync: ${lastSync}`);

    // query delta and update cache
    const deltaOptions: QueryOptions<any> = {
      query: this.syncMap[key].base.query,
      variables: { ...this.syncMap[key].base.variables, lastSync},
      fetchPolicy: 'no-cache',
    };
    if (nextToken) {
      delete deltaOptions.variables.lastSync;
      deltaOptions.variables.nextToken = nextToken;
    }
    const query = this.client.query<T>(deltaOptions);
    if (countPaginate === undefined) {
      countPaginate = this.syncMap[key].base.limitPaginate;
    }
    query.then((resp) => this._getUpdateFn(key, countPaginate as number)(undefined, resp))
    .catch(e => log.warn(`delta for ${key} failed`));
    return from(query);
  }

  /** throw a fetchDelta (sync query) for each key given. If no keys, fetch delta for all keys in config.
   * keys are used to keep flexibility. It can be used like this:
   *
   * x.fetchManyDeltas(['Product', 'Supplier'])
   */
  fetchManyDeltas(
    keys?: string[]
  ): Observable<ApolloQueryResult<any> | null>[] {
    if (!keys) {
      keys = Object.keys(this.syncMap);
    }
    const allFetchDeltas = keys.map((key) => this.fetchDelta(key));
    return allFetchDeltas;
  }

  private _getUpdateFn(key: string, countPaginate: number): any {
    return (_: any, respServer: any): void => {
      const { items, startedAt, nextToken } = Object.values(respServer.data)[0] as any;
      if (startedAt === undefined || nextToken === undefined) {
        throw Error(`the base query should contain the field "startedAt" & "nextToken"`);
      }
      // update cache with entities modified
      this.apiCache.upsert(key, items);
      this._updateLastSync(key, startedAt);

      if (nextToken && countPaginate) {
        this.fetchDelta(key, nextToken, countPaginate - 1)
      }
    };
  }

  /** update lastSync in cache for specific key.
   * @param key: used to find the correct sync query
   * @param newLastSync: is optional. If not provided, return the current lastSync from cache
   */
  private _updateLastSync(key: string, newLastSync?: number) {
    const { query, variables } = this.syncMap[key].base;
    const queryName = GqlHelper.getQueryName(query);
    const readResp: CacheData | null = this.client.readQuery({
      query,
      variables,
    });
    if (!readResp) {
      return;
    } else if (newLastSync) {
      // we set the new lastSync into the cache
      readResp[queryName].startedAt = newLastSync;
      this.client.writeQuery({
        data: readResp,
        query,
        variables,
      });
    } else {
      // we get the old lastSync
      newLastSync = (readResp && readResp[queryName]?.startedAt) || undefined;
    }
    return newLastSync;
  }

  private removeOldSubscriptions() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
    this.subscriptions = [];
  }
}
