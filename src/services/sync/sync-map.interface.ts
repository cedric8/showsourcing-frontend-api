import { DocumentNode } from 'graphql';

export interface BaseQueryOptions {
  query: DocumentNode;
  __typename: string;
  baseRefreshIntervalInSeconds?: number;
  variables?: any;
  update?: (proxyCache: any, respServer: any) => void;
  limitPaginate: number;
}

export interface SyncMap {
  [key: string]: {
    syncable: boolean;
    base: Omit<BaseQueryOptions, 'update'>;
    // subscriptions?: Omit<BaseQueryOptions, 'update' | 'baseRefreshIntervalInSeconds'>; // not implemented yet
  };
}
