import { ApiFilter } from './api-filter.class';
import { SortType, PaginationType } from './filter.type';

let filter: ApiFilter;
beforeAll(() => {
  filter = new ApiFilter();
});

/** ======================
 *  ======= SORT =========
 *  ======================
 */

describe('Api Filter service: sort()', () => {
  // ASC
  it('should sort with direction \'ASC\' on specific property', () => {
    const items = [{ name: 'acb' }, { name: 'abc' }, { name: 'bac' }];
    const sort: SortType = { direction: 'ASC', property: 'name' };
    const resp = filter.sort(items, sort);
    expect(resp[0].name).toBe('abc');
    expect(resp[1].name).toBe('acb');
  });

  // DESC
  it('should sort with direction \'DESC\' on specific property', () => {
    const items = [{ name: 'acb' }, { name: 'abc' }, { name: 'bac' }];
    const sort: SortType = { direction: 'DESC', property: 'name' };
    const resp = filter.sort(items, sort);
    expect(resp[0].name).toBe('bac');
    expect(resp[1].name).toBe('acb');
  });

  it('should sort numbers on specific property', () => {
    const items = [{ n: 5 }, { n: 10 }, { n: 1 }, { n: 8 }];
    const sort: SortType = { direction: 'ASC', property: 'n' };
    const resp = filter.sort(items, sort);
    expect(resp[0].n).toBe(1);
    expect(resp[1].n).toBe(5);
    expect(resp[2].n).toBe(8);
  });
});

/** ======================
 *  ===== PAGINATE =======
 *  ======================
 */
describe('Api Filter service: paginate()', () => {
  let items: any;
  beforeAll(() => {
    items = [{ p: '1' }, { p: '1' }, { p: '2' }, { p: '2' }, { p: '3' }];
  });
  it('should paginate', () => {
    const pagination: PaginationType = { page: 1, limit: 2 };
    const resp = filter.paginate(items, pagination);
    expect(resp.length).toBe(2);
    resp.forEach((item) => expect(item.p).toBe('2'));
  })

  it('should give all items when limit = 0', () => {
    const pagination: PaginationType = { page: 1, limit: 0 };
    const resp= filter.paginate(items, pagination);
    expect(resp.length).toBe(5);
  });
});

/** ======================
 *  ====== FILTER ========
 *  ======================
 */
describe('Api Filter service: filter()', () => {
  // no filters
  it('should find all items (without filters object)', () => {
    const resp = filter.filter<any>([{}, {}, {}], undefined);
    expect(resp.length).toBe(3);
  });

  // eq?: number;
  it('should filter items with property \'eq\' (number)', () => {
    const items = [{ age: 89 }, { age: 5 }, { age: 20 }];
    const filters = { property: 'age', eq: 89 };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(1);
    expect(resp[0].age).toBe(89);
  });

  // ne?: number;
  it('should filter items with property \'ne\' (number)', () => {
    const items = [{ age: 89 }, { age: 5 }, { age: 20 }];
    const filters = { property: 'age', ne: 89 };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(2);
    expect(resp.find((item) => item.age === 89)).toBeFalsy();
  });

  // ge?: number;
  it('should filter items with property \'ge\' (number)', () => {
    const items = [{ age: 89 }, { age: 5 }, { age: 20 }, { age: 3 }];
    const filters = { property: 'age', ge: 20 };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(2);
    expect(resp.find((item) => item.age === 20)).toBeTruthy();
    expect(resp.find((item) => item.age === 89)).toBeTruthy();
  });

  // gt?: number;
  it('should filter items with property \'gt\' (number)', () => {
    const items = [{ age: 89 }, { age: 5 }, { age: 20 }, { age: 3 }];
    const filters = { property: 'age', gt: 20 };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(1);
    expect(resp[0].age).toBe(89);
  });

  // le?: number;
  it('should filter items with property \'le\' (number)', () => {
    const items = [{ age: 89 }, { age: 5 }, { age: 20 }, { age: 3 }];
    const filters = { property: 'age', le: 5 };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(2);
    expect(resp.find((item) => item.age === 5)).toBeTruthy();
    expect(resp.find((item) => item.age === 3)).toBeTruthy();
  });

  // lt?: number;
  it('should filter items with property \'lt\' (number)', () => {
    const items = [{ age: 89 }, { age: 5 }, { age: 20 }, { age: 3 }];
    const filters = { property: 'age', lt: 5 };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(1);
    expect(resp.find((item) => item.age === 3)).toBeTruthy();
  });

  // contains?: string;
  it('should filter items with property \'contains\' (not case sensitive)', () => {
    const items = [{ name: 'Florent' }, { name: 'Louis' }, { name: 'Amelia' }];
    const filters = { property: 'name', contains: 'lo' };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(2);
    expect(resp.find((item) => item.name === 'Amelia')).toBeFalsy();
  });

  // notContains?: string;
  it('should filter items with property \'notContains\' (not case sensitive)', () => {
    const items = [{ name: 'Florent' }, { name: 'Louis' }, { name: 'Amelia' }];
    const filters = { property: 'name', notContains: 'lo' };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(1);
    expect(resp[0].name).toBe('Amelia');
  });

  // beginsWith?: string;
  it('should filter items with property \'beginsWith\' (no case sensitive)', () => {
    const items = [{ name: 'Florent' }, { name: 'Louis' }, { name: 'Amelia' }];
    const filters = { property: 'name', beginsWith: 'lo' };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(1);
    expect(resp[0].name).toBe('Louis');
  });

  // inStrings?: string[];
  it('should filter items with property \'inStrings\'', () => {
    const items = [{ id: 'uuid1' }, { id: 'uuid2' }, { id: 'uuid3' }];
    const filters = { property: 'id', inStrings: ['uuid2', 'uuid3'] };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(2);
    expect(resp.find((item) => item.id === 'uuid2')).toBeTruthy();
    expect(resp.find((item) => item.id === 'uuid3')).toBeTruthy();
  });

  // isString?: string;
  it('should filter items with property \'isString\' (case sensitive !)', () => {
    const items = [{ name: 'Florent' }, { name: 'Louis' }, { name: 'Amelia' }];
    // lowercase
    const filtersLower = { property: 'name', isString: 'louis' };
    const respLower = filter.filter<any>(items, filtersLower);
    expect(respLower.length).toBe(0);

    // Uppercase
    const filtersUpper = { property: 'name', isString: 'Louis' };
    const respUpper = filter.filter<any>(items, filtersUpper);
    expect(respUpper.length).toBe(1);
    expect(respUpper[0].name).toBe('Louis');
  });

  // exists?: boolean;
  it('should filter items with property \'exists\'', () => {
    const items = [
      { id: 'uuid1' },
      { name: 'Alain' },
      { id: 'uuid2', age: 36 },
    ];
    const filtersNoId = { property: 'id', exists: false };
    const respNoId = filter.filter<any>(items, filtersNoId);
    expect(respNoId.length).toBe(1);
    expect(respNoId[0].name).toBe('Alain');

    const filtersAge = { property: 'age', exists: true };
    const respAge = filter.filter<any>(items, filtersAge);
    expect(respAge.length).toBe(1);
    expect(respAge[0].age).toBe(36);
  });

  // isTrue?: boolean;
  it('should filter items with property \'isTrue\' (boolean)', () => {
    const items = [
      { favori: false, id: 'uuid1' },
      { id: 'uuid2', favori: false },
      { id: 'uuid3', favori: true },
    ];
    const filters = { property: 'favori', isTrue: true };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(1);
    expect(resp[0].id).toBe('uuid3');
  });

  // and?: FilterType[];
  it('should filter items with property \'and\'', () => {
    const items = [{ age: 89 }, { age: 5 }, { age: 20 }];
    const filters = {
      and: [
        { property: 'age', ge: 5 },
        { property: 'age', le: 20 },
      ],
    };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(2);
    expect(resp.find((item) => item.age === 5)).toBeTruthy();
    expect(resp.find((item) => item.age === 20)).toBeTruthy();
  });

  // or?: FilterType[];
  it('should filter items with property \'or\'', () => {
    const items = [{ age: 89 }, { age: 5 }, { age: 20 }];
    const filters = {
      or: [
        { property: 'age', eq: 5 },
        { property: 'age', eq: 20 },
      ],
    };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(2);
    expect(resp.find((item) => item.age === 5)).toBeTruthy();
    expect(resp.find((item) => item.age === 20)).toBeTruthy();
  });

  // nested and
  it('should filter items with properties and/or nested', () => {
    const items = [{ age: 89 }, { age: 5 }, { age: 20 }];
    const filters = {
      and: [
        { property: 'age', gt: 10 },
        {
          or: [
            { property: 'age', eq: 5 },
            { property: 'age', eq: 20 },
          ],
        },
      ],
    };
    const resp = filter.filter<any>(items, filters);
    expect(resp.length).toBe(1);
    expect(resp.find((item) => item.age === 20)).toBeTruthy();
  });
});
