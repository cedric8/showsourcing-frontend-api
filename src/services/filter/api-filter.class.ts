// ? which behavior if property doesn't exist on entity ?
// - if pass automatically, we can't do isString
// - if don't pass automatically, why we have "exists" ?
import { SortType, FilterType, PaginationType } from './filter.type';

/** ApiFilter is used in api-db.
 * it help ot process an array of entities with
 * filter, sort and paginate methods
 */
export class ApiFilter {
  filter<T>(items: T[], filter?: FilterType): T[] {
    if (!filter || !Object.keys(filter).length) {
      return items;
    }
    return items.filter(item => this._applyFilter(item, filter));
  }

  sort<T>(items: T[], sort?: SortType): T[] {
    if (!sort) {
      return items;
    }
    // used to get the opposite of the sort return (if DESC)
    const sortDirection = sort.direction === 'ASC' ? 1 : -1;
    const sortProperty = sort.property;

    return items.sort((d1: any, d2: any) => {
      let ret = 0;
      if (d1[sortProperty] < d2[sortProperty]) {
        ret = -1;
      } else if (d1[sortProperty] > d2[sortProperty]) {
        ret = 1;
      }
      return ret * sortDirection;
    });
  }

  paginate(items: any[], pagination?: PaginationType) {
    if (!pagination) {
      return items;
    }
    pagination.limit = Number(pagination.limit)
    pagination.page = Number(pagination.page)
    const skip = pagination.page * pagination.limit;
    const limit = pagination.limit ? pagination.limit + skip : items.length;
    return items.slice(skip, limit);
  }

  private _applyFilter(item: any, filter: any) {
    if (!filter) {
      return true;
    }
    const propertyName = filter.property;

    // OR Clauses
    if (filter.or !== undefined) {
      let pass = false;
      // If any of these passes, then we are good.
      // Otherwise we reject the OR if all negative.
      for (const subFilter of filter.or) {
        if (this._applyFilter(item, subFilter)) {
          pass = true;
          break;
        }
      }
      if (!pass) {
        return false;
      }
    }

    // AND Clauses
    if (filter.and !== undefined) {
      // If any of these fails, then we reject.
      for (const subFilter of filter.and) {
        if (!this._applyFilter(item, subFilter)) {
          return false;
        }
      }
    }

    if (!propertyName) {
      return true;
    }
    const property = item[propertyName];

    // Contains
    // Not Case Sensitive
    if (filter.contains !== undefined) {
      if (typeof property === 'string' || property instanceof String) {
        if (!property.toLowerCase().includes(filter.contains.toLowerCase())) {
          return false;
        }
      }
    }
    // Not Contains
    // Not Case Sensitive
    if (filter.notContains !== undefined) {
      if (typeof property === 'string' || property instanceof String) {
        if (property.toLowerCase().includes(filter.notContains.toLowerCase())) {
          return false;
        }
      }
    }

    // BeginsWith
    // Not Case Sensitive
    if (filter.beginsWith !== undefined) {
      if (typeof property === 'string' || property instanceof String) {
        if (
          !property.toLowerCase().startsWith(filter.beginsWith.toLowerCase(), 0)
        ) {
          return false;
        }
      }
    }

    // Check Array of Strings
    // Case Sensitive
    if (filter.inStrings !== undefined) {
      if (typeof property === 'string' || property instanceof String) {
        if (!filter.inStrings.includes(property)) {
          return false;
        }
      }
    }

    // Check exact value of Strings
    // Case Sensitive
    if (filter.isString !== undefined) {
      if (filter.isString !== property) {
        return false;
      }
    }

    // isTrue
    // Boolean Check === is True
    if (filter.isTrue !== undefined) {
      if (typeof property === 'boolean') {
        if (property !== filter.isTrue) {
          return false;
        }
      }
    }
    // EQ - Number Equality
    if (filter.eq !== undefined) {
      if (typeof property === 'number' || property instanceof Number) {
        if (filter.eq !== property) {
          return false;
        }
      }
    }
    // NE - Number Not Equality
    if (filter.ne !== undefined) {
      if (typeof property === 'number' || property instanceof Number) {
        if (filter.ne === property) {
          return false;
        }
      }
    }
    // LE - Number LESS or EQUAL
    if (filter.le !== undefined) {
      if (typeof property === 'number' || property instanceof Number) {
        if (!(property <= filter.le)) {
          return false;
        }
      }
    }
    // LT - Number LESS THEN
    if (filter.lt !== undefined) {
      if (typeof property === 'number' || property instanceof Number) {
        if (!(property < filter.lt)) {
          return false;
        }
      }
    }
    // GE - Number GREATER OR EQUAL
    if (filter.ge !== undefined) {
      if (typeof property === 'number' || property instanceof Number) {
        if (!(property >= filter.ge)) {
          return false;
        }
      }
    }
    // GT - Number GREATER THAN
    if (filter.gt !== undefined) {
      if (typeof property === 'number' || property instanceof Number) {
        if (!(property > filter.gt)) {
          return false;
        }
      }
    }
    // Exists
    if (filter.exists !== undefined) {
      if (filter.exists === true && property === undefined) {
        return false;
      } else if (filter.exists === false && property !== undefined) {
        return false;
      }
    }
    return true;
  }
}
