// pagination
export type PaginationType = {
  page: number;
  limit: number;
};

export type SortType = {
  direction: 'ASC' | 'DESC';
  property: string;
};

export type FilterType = {
  property?: string;
  eq?: number;
  ne?: number;
  ge?: number;
  gt?: number;
  le?: number;
  lt?: number;
  contains?: string;
  notContains?: string;
  beginsWith?: string;
  inStrings?: string[];
  isString?: string;
  exists?: boolean;
  isTrue?: boolean;
  and?: FilterType[];
  or?: FilterType[];
};
