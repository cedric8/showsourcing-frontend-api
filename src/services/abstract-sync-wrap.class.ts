import AWSAppSyncClient from 'aws-appsync';


export class AbstractAppsyncWrap {
  private _client: AWSAppSyncClient<any> | undefined;
  setAppSyncClient(client: AWSAppSyncClient<any>) {
    this._client = client
  }

  get client() {
    if (!this._client) {
      throw Error(`The client isn't ready, wait for it to be ready with ready$`);
    }
    return this._client;
  }
}